# DS+A

A silly project for implementing different data structures in [Zig][zig-docs],
when Santtu is bored and has nothing better to do.

[zig-docs]: https://ziglang.org/documentation/0.10.1/

## License

This project uses the MIT license. See the attached [`LICENSE`](./LICENSE) file
for full details.
